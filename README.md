## Esto es [Co-Workers](https://coworkers-server-x3q4dlgumq-uc.a.run.app/)

Una aplicación hecha con el framework [Next.js](https://nextjs.org/) donde podrás:

- Crear una cuenta de usuario con un correo poder ingresar a la aplicación.
- Generar equipos aleatorios de trabajo
- Ver las características de cada trabajador.

## Para probar en desarrollo

Primero, corre el servidor de desarrollo:

```bash
npm run dev
```

Abre [http://localhost:3030](http://localhost:3030) en el browser para ver el resultado.

## Para instalar localmente

```bash

npm install

npm run build

npm start

```

## Para desplegar con cloud run

Este es el comando para publicar en producción con gcp

```bash
gcloud builds submit . --config=cloudbuild.yaml
```

## Para ejecutar los test

```bash
 npm run test
```

## Para docker local

1.Crear el tag local

```bash
docker build --tag coworkers-challenge:01 .
```

2.Correr la instancia local

```bash
docker run -p 3333:3333 coworkers-challenge:01
```

3.Para ver el nuevo Image ID creado

```bash
docker images
```

4.Asignar el latest docker tag

```bash
docker tag 19ad5dfd7019 gcr.io/coworkers-challenge/coworkers-server
```

## Otras consideraciones

- Ver listado de configuraciones

```bash
  gcloud config list
```

- Set del proyecto gcp command - sdk google

```bash
  gcloud config set project coworkers-challenge
```

- Autenticación de docker

```bash
  gcloud auth configure-docker
```

- Bajar un contendor

```bash
  docker rm -f gcr.io/coworkers-challenge/coworkers-server
```

- Ver listado de contenedores

```bash
  docker container ls
```
