// import App from 'next/app';
import React from 'react';
import PropTypes from 'prop-types';
import firebase, { FirebaseContext } from '../firebase';
import useAutentication from '../firebase/autentication/useAutentication';
import CoworkersProvider from '../components/coworkers/CoworkersContext';
const MyApp = props => {
    const user = useAutentication();
    const { Component, pageProps } = props;

    return (
        <CoworkersProvider>
            <FirebaseContext.Provider
                value={{
                    firebase,
                    user
                }}
            >
                <Component {...pageProps} />
            </FirebaseContext.Provider>
        </CoworkersProvider>
    )
}

MyApp.propTypes = {
    Component:PropTypes.any,
    pageProps:PropTypes.object.isRequired
}

export default MyApp;