import React, { useState } from 'react';
import styled from '@emotion/styled';
import Router from 'next/router';
import Layout from '../components/layout/Layout';
import { Form, Field, InputSubmit, Error } from '../components/shared/Form';
import firebase from '../firebase';
// validaciones
import useValidacion from '../validations/useValidacion';
import validarIniciarSesion from '../validations/validarIniciarSesion';

const STATE_INICIAL = {
  email: '',
  password: ''
}

const ContainerTitle = styled.h1`
  text-align: center;
  margin-top: 5rem; 
`;

const Login = () => {

  const [error, guardarError] = useState(false);

  const { valores, errores, handleSubmit, handleChange, handleBlur } = useValidacion(STATE_INICIAL, validarIniciarSesion, iniciarSesion);

  const { email, password } = valores;

  async function iniciarSesion() {
    try {
      await firebase.login(email, password);
      Router.push('/workers-list');
    } catch (error) {
      console.error('Hubo un error al autenticar el usuario ', error.message);
      guardarError(error.message);
    }
  }


  return (
    <div>
      <Layout>
        <>
          <ContainerTitle>Iniciar Sesión
          </ContainerTitle>
          <Form
            onSubmit={handleSubmit}
            noValidate
          >
            <Field>
              <label htmlFor="email">Email</label>
              <input
                type="email"
                id="email"
                placeholder="Tu Email"
                name="email"
                value={email}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Field>
            {errores.email && <Error>{errores.email}</Error>}

            <Field>
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                placeholder="Tu Password"
                name="password"
                value={password}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Field>
            {errores.password && <Error>{errores.password}</Error>}

            {error && <Error>{error} </Error>}

            <InputSubmit
              type="submit"
              value="Iniciar Sesión"
            />
          </Form>
        </>
      </Layout>
    </div>
  )
}

export default Login