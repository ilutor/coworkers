import React, { useState } from 'react';
import styled from '@emotion/styled';
import Router from 'next/router';
import Layout from '../components/layout/Layout';
import { Form, Field, InputSubmit, Error } from '../components/shared/Form';
import firebase from '../firebase/firebase';
// validaciones
import useValidacion from '../validations/useValidacion';
import validarCrearCuenta from '../validations/validarCrearCuenta';

const INITIAL_STATE = {
  nombre: '',
  email: '',
  password: ''
}

const ContainerTitle = styled.h1`
  text-align: center;
  margin-top: 5rem; 
`;

const CrearCuenta = () => {

  const [error, saveError] = useState(false);

  const { valores, errores, handleSubmit, handleChange, handleBlur } = useValidacion(INITIAL_STATE, validarCrearCuenta, crearCuenta);

  const { nombre, email, password } = valores;

  async function crearCuenta() {
    try {
      await firebase.registrar(nombre, email, password);
      Router.push('/');
    } catch (error) {
      console.error('Hubo un error al crear el usuario ', error.message);
      saveError(error.message);
    }
  }

  return (
    <div>
      <Layout>
        <>
          <ContainerTitle>
            Crear Cuenta
          </ContainerTitle>
          <Form
            onSubmit={handleSubmit}
            noValidate
          >
            <Field>
              <label htmlFor="nombre">Nombre</label>
              <input
                type="text"
                id="nombre"
                placeholder="Tu Nombre"
                name="nombre"
                value={nombre}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Field>

            {errores.nombre && <Error>{errores.nombre}</Error>}

            <Field>
              <label htmlFor="email">Email</label>
              <input
                type="email"
                id="email"
                placeholder="Tu Email"
                name="email"
                value={email}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Field>
            {errores.email && <Error>{errores.email}</Error>}

            <Field>
              <label htmlFor="password">Password</label>
              <input
                type="password"
                id="password"
                placeholder="Tu Password"
                name="password"
                value={password}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </Field>
            {errores.password && <Error>{errores.password}</Error>}

            {error && <Error>{error} </Error>}

            <InputSubmit
              type="submit"
              value="Crear Cuenta"
            />
          </Form>
        </>
      </Layout>
    </div>
  )
}

export default CrearCuenta