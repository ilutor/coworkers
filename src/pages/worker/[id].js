import React from 'react';
import { useRouter } from 'next/router';
import Layout from '../../components/layout/Layout';
import CoworkerDetail from '../../components/coworkers/CoworkerDetail';

const Worker = () => {

    // Routing para obtener el id actual
    const router = useRouter();
    const { query: { id } } = router;

    return (
        <>
            <Layout>
                <CoworkerDetail
                    id={id}
                />
            </Layout>
        </>
    );
}

export default Worker;