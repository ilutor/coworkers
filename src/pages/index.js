import React from 'react';
import Layout from '../components/layout/Layout';
import Banner from '../components/layout/Banner';

const Index = () => {
  return (
    <div>
      <Layout>
        <Banner />
      </Layout>
    </div>
  );
}

export default Index;