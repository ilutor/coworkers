import React from 'react';
import Layout from '../components/layout/Layout';
import CoworkersList from '../components/coworkers/CoworkersList';

const WorkersList = () => {
    return (
        <>
            <Layout>
                <CoworkersList />
            </Layout>
        </>
    );
}

export default WorkersList;