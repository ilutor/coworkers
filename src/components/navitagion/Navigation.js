import React, { useContext } from 'react';
import Link from 'next/link'
import { useRouter } from "next/router";
import styled from '@emotion/styled';
import { FirebaseContext } from '../../firebase';

const Nav = styled.nav`
    padding-left:2rem;
    border:none;
    ul{
        display:flex;        
    }
    a{
        font-size:1.8rem;
        margin-left:2rem;
        color:var(--grey);
        font-family: 'Fira Sans', sans-serif;
        &:last-of-type{
            margin-right:0;
        }
        &:active,&:hover{
            color :var(--grey3);
        }
        
    }
    .active > a {
        color :var(--grey3);
        text-decoration:underline;
    }
`;

const Navigation = () => {

    const { user } = useContext(FirebaseContext);

    const router = useRouter();

    return (
        <Nav>
            <ul>
                <li className={router.pathname == "/" ? "active" : ""}>
                    <Link href="/">
                        <a>Inicio</a>
                    </Link>
                </li>
                {user ? (
                    <li className={router.pathname == "/workers-list" ? "active" : ""}>
                        <Link href="/workers-list">
                            <a>Listado</a>
                        </Link>
                    </li>
                ) : <></>}
            </ul>
        </Nav>
    );
}

export default Navigation;