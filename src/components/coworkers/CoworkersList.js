import React, {useContext } from 'react';
import { CoworkersContext } from './CoworkersContext';
import CoworkerCard from './CoworkerCard';
import styled from '@emotion/styled';

const CardsContainer = styled.div`
  display:grid;
  grid-template-columns:auto auto auto;
  text-align: center;
  @media (max-width:1024px) {
        display:flex;
        flex-direction:column;
        column-gap: 2rem;
   }
`;

const TitlesContainer = styled.h2`
  display:flex;
  justify-content:center;
  box-shadow: 5px 5px 5px hsla(170, 29%, 90%, 1);
  background-color:hsla(170,29%,60%,1);
  @media (max-width:768px) {
       font-size: 3rem;
  }
`;

const CoworkersList = () => {

    const { coworkers } = useContext(CoworkersContext);

    return (
        <>
            <TitlesContainer>
                Listado de colaboradores
            </TitlesContainer>
            <CardsContainer>
                {coworkers.map(coworker => (
                    <CoworkerCard
                        key={coworker.login.uuid}
                        coworker={coworker}
                    >
                    </CoworkerCard>
                ))}
            </CardsContainer>
        </>
    );
}

export default CoworkersList;