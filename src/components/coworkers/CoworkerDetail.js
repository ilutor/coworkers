import React, { useContext, useState, useEffect } from 'react';
import Link from 'next/link';
import styled from '@emotion/styled';
import FontAwesome from 'react-fontawesome';
import PropTypes from 'prop-types';
import { CoworkersContext } from './CoworkersContext';
import Error404 from '../layout/404';


const CoworkerDetailContainer = styled.div`
    text-align:center;
    display:flex;
    justify-content:center;
    margin:10px;
    padding:20px;
`;

const CoworkerDataContainer = styled.div`
    color:var(--grey);
    text-align:center;
    width:50%;
    margin:10px;
    padding:20px;
    border-radius:30px;    
    box-shadow: 5px 5px 5px hsla(237, 20%, 76%, 0.55),
    -5px -5px 5px hsla(237,29%,88%,0.55);

    @media (max-width:768px) {
        width:100%;
    }
`;

const Image = styled.img`
  border-radius:150px;
  border: 3px solid hsla(237, 27%, 87%, 1);
  align-self: center;
  margin:10px;
  box-shadow: 7px 7px 7px #d8d8e9fc,
  -7px -7px 7px #d8d8e9fc;
`;

const Button = styled.button`
    margin-top:20px;
    background-color:var(--grey3);
    color:var(--orange);
    padding:10px;
    font-size:2rem;
    border-radius:25px;
    cursor: pointer;
    border:unset;
    box-shadow: 5px 5px 5px hsla(237, 20%, 76%, 0.55),
    -5px -5px 5px hsla(237,29%,88%,0.55);
    transition: all 0.3s ease-in-out;
    &:hover,&:active{    
            transition-delay:1s;
            color: hsla(237, 23%, 15%, 1);
            box-shadow: inset -5px -5px 5px hsla(237,29%,88%,0.55), 
            inset 5px 5px 5px hsla(237,20%,74%,0.55);
    }
    /*aqui estilos de boton soft */    
`;

const PersonName = styled.div`
  font-size:2rem;
`;

const CoworkerDetail = ({ id }) => {

    const { coworkers } = useContext(CoworkersContext);

    const [error, saveError] = useState(false);

    const [coworker, saveCoworker] = useState([]);

    const getCoworker = () => {
        const firstCoworker = coworkers.filter(coworker => coworker.login.uuid === id)[0];
        if (firstCoworker != null) {
            saveCoworker(firstCoworker);
        } else {
            saveError(true);
            console.log(error);
        }
    }

    useEffect(() => {
        getCoworker();
        // eslint-disable-next-line
    }, []);

    if (Object.keys(coworker).length === 0 && !error) return 'Cargando...';

    const { login, picture, name, email, phone, location, registered } = coworker;

    const formatDate = (string) => {
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        return new Date(string).toLocaleDateString([], options);
    }

    return (
        <>
            {error ? (
                <Error404 />
            ) : <>
                    <CoworkerDetailContainer>
                        <CoworkerDataContainer>
                            <Image src={picture.large} />
                            <PersonName>
                                {name.first} {name.last}
                            </PersonName>
                            <div>
                                {email}  <FontAwesome name="envelope" />
                            </div>
                            <div>
                                {phone} <FontAwesome name="phone" />
                            </div>
                            <div>
                                {login.username} <FontAwesome name="user" />
                            </div>
                            <div>
                                {login.password} <FontAwesome name="eye" />
                            </div>
                            <div>
                                {location.street.number}  {location.street.name} <FontAwesome name="map" />
                            </div>
                            <div>
                                Registrado el {formatDate(registered.date)} <FontAwesome name="address-card" />
                            </div>
                            <div>
                                <Link href="/workers-list">
                                    <Button>Volver al listado</Button>
                                </Link>
                            </div>
                        </CoworkerDataContainer>
                    </CoworkerDetailContainer>
                </>}
        </>
    );
}

CoworkerDetail.propTypes = {
    id: PropTypes.string.isRequired
}
export default CoworkerDetail;