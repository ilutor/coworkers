import React, { createContext, useState, useEffect } from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';
//Crear el context
export const CoworkersContext = createContext();

//Provider es donde se encuentan las funciones y state
const CoworkersProvider = (props) => {
    
    const [coworkers, saveCoworkers] = useState([]);
    
    const getCoworkers = async () => {
        const USERS_AMOUNT = 15;
        const url = `https://randomuser.me/api/?results=${USERS_AMOUNT}`;
        const result = await axios.get(url);
        console.log(url);
        saveCoworkers(result.data.results);
    }

    useEffect(() => {
        getCoworkers();
        // eslint-disable-next-line
    }, []);

    return (
        <CoworkersContext.Provider
            value={{
                coworkers
            }}
        >
            {props.children}
        </CoworkersContext.Provider>
    )

}

CoworkersProvider.propTypes = {
    children:PropTypes.object.isRequired
}
export default CoworkersProvider;