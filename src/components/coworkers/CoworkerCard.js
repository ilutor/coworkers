import React from 'react';
import styled from '@emotion/styled';
import FontAwesome from 'react-fontawesome';
import Link from 'next/link';
import PropTypes from 'prop-types';

const ContainerCard = styled.div`
  /* border:1px solid black; */
  padding: 10px 100px 10px 100px;
  width:100%;
  @media screen {
    padding: 10px 10px 10px 10px;    
  }
`;
const Card = styled.div`
  display:flex;
  padding-top:3px;
  padding-bottom:3px;
  margin:5px;
  margin-right:30px;
  flex-direction:row;  
  background-color:var(--grey3);
  justify-content: space-between;
  border-radius:15px;
  box-shadow: 5px 5px 5px hsla(237, 20%, 76%, 0.55),
  -5px -5px 5px hsla(237,29%,88%,0.55);
  transition: all 0.3s ease-in-out;
  &:hover,&:active{    
        transition-delay:10s;
        color: hsla(237, 23%, 15%, 1);
        box-shadow: inset -5px -5px 5px hsla(237,29%,88%,0.55), 
        inset 5px 5px 5px hsla(237,20%,74%,0.55);
  }

  @media screen {
    margin-right: 10px;    
  }
`;

const CardData = styled.div`
  color:var(--grey);
  display:flex;
  flex-direction:column;
  text-align:right;
  margin-right:30px;
  
`;

const Image = styled.img`
  border-radius:180px;
  border: 1.5px solid hsla(237, 27%, 87%, 1);
  align-self: center;
  margin:10px;
  box-shadow: 7px 7px 7px #d8d8e9fc,
  -7px -7px 7px #d8d8e9fc;
  
`;

const PersonName = styled.div`
  font-size:2rem;
`;

const CoworkerCard = ({ coworker }) => {
  const { login, picture, name, email, phone } = coworker;
  return (
    <ContainerCard>
      <Link href="/worker/[id]" as={`/worker/${login.uuid}`} >
        <a>
          <Card>
            <div>
              <Image src={picture.thumbnail} />
            </div>
            <CardData>
              <PersonName>
                {name.first} {name.last}
              </PersonName>
              <div>
                {email} <FontAwesome name="envelope" />
              </div>
              <div>
                {phone} <FontAwesome name="phone"/>
              </div>
            </CardData>
          </Card>
        </a>
      </Link>
    </ContainerCard>
  );
}

CoworkerCard.propTypes = {
  coworker: PropTypes.object.isRequired
}

export default CoworkerCard;