import React from 'react';
import styled from '@emotion/styled';
import CallToAction from './CallToAction';

const BannerContainer = styled.div`    
    background-image:url('/static/img/coworkers.jpg');
    background-repeat:no-repeat; 
    position:fixed;
    right:0;
    bottom:0;
    margin:0;
    top:0;
    width:100%;
    height:100%;
    z-index:-10;
    background-size:100%;

    @media (max-width:768px) {
        background-image:unset;
        background-color: hsla(223, 40%, 60%, 1);
    }

`;

const Banner = () => {
    return (
        <>
            <BannerContainer>
                <CallToAction></CallToAction>
            </BannerContainer>
        </>
    );
}

export default Banner;