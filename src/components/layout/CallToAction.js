import React, { useContext } from 'react';
import Link from 'next/link';
import styled from '@emotion/styled';
import { FirebaseContext } from '../../firebase';

const Button = styled.button`
    background-color:var(--orange);
    color:var(--grey3);
    padding:10px;
    font-size:2rem;
    border-radius:25px;
    border:unset;
    cursor: pointer;
    /*aqui estilos de boton soft */      
    box-shadow: 5px 5px 5px hsla(240, 2%, 58%, 0.7),
     -5px -5px 5px hsla(13, 43%, 93%, 0.7);
    transition: all 0.3s ease-in-out;
    &:hover,&:active{    
            transition-delay:5s;
            box-shadow: inset -5px -5px 5px hsla(237, 28%, 40%, 1),
             inset 5px 5px 5px hsl(237,43%,21%);
    }
    @media screen {     
        margin-top: 50px;   
    } 
`;

const CallToActionContainer = styled.div`
    margin-top: 15%;
    padding-top: 100px;
    padding-bottom: 100px;
    text-align:center;
    width: 100%;
    background-color: #ffffff8c;
    @media (max-width:768px) {
        height: 100%;
    }
`;

const ImageContainer = styled.img`
    height:40px;
`;

const CallToAction = () => {

    const { user } = useContext(FirebaseContext);

    return (
        <CallToActionContainer>
            <h1>Generemos equipos de trabajo aleatorios ! <ImageContainer src="static/img/comentario.png"/></h1> 
           
            {!user ? (
                <Link href="/login">
                    <a>
                        <Button>Ingresa para ver los equipos</Button>
                    </a>
                </Link>
            ) : (
                <Link href="/workers-list">
                    <a>
                        <Button>Ingresa para ver los equipos</Button>
                    </a>
                </Link>
            )}
        </CallToActionContainer>
    );
}

export default CallToAction;