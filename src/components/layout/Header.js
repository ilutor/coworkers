import React, { useContext } from 'react';
import Link from 'next/link';
import Router from 'next/router';
import styled from '@emotion/styled';
import Navigation from '../navitagion/Navigation'
import Button from '../shared/Button';
import { FirebaseContext } from '../../firebase';

const HeaderContainer = styled.div`
    max-width:1200px;
    width:95%;
    margin:0 auto;
    @media (min-width:768px){
        display:flex;
        justify-content:space-between;
    }
`;

const HeaderSeparator = styled.div`
    padding:1rem 0;    
    background-color:hsla(170, 29%, 60%, 1);
`;

const LoginContainer = styled.div`
    display:flex;
    align-items:center;
`;

const Logo = styled.p`
    color:var(--orange);
    font-size:4rem;
    line-height:0;
    font-family: 'Roboto Condensed', serif;
    font-weight:700;
    margin-right:2rem;
    @media screen {
        font-size: 2.5rem;   
    }
`;

const ButtonsContainer = styled.div`
    display:flex;
    align-items:center;
`;

const Grettings = styled.p`
    margin-right:2rem;
    color: var(--grey);
`;

const Header = () => {

    const { user, firebase } = useContext(FirebaseContext);

    async function closeSession() {
        try {
            await firebase.cerrarSesion();
            Router.push('/');
        } catch (error) {
            console.error('Hubo un error al cerrar sesión', error.message);
        }
    }


    return (
        <HeaderSeparator>
            <HeaderContainer>
                <LoginContainer>
                    <Link href="/">
                        <a>
                            <Logo>Co-Workers</Logo>
                        </a>
                    </Link>
                    <Navigation />
                </LoginContainer>
                <ButtonsContainer>
                    {user ? (
                        <>
                            <Grettings>
                                Hola: {user.displayName}
                            </Grettings>
                            <Button
                                bgColor="true"
                                onClick={closeSession}
                            >
                                Cerrar Sesión
                            </Button>
                        </>
                    ) : (
                            <>
                                <Link href="/login">
                                    <Button
                                        bgColor="true"
                                    >Login</Button>
                                </Link>
                                <Link href="/crear-cuenta">
                                    <Button>Crear Cuenta</Button>
                                </Link>
                            </>
                        )}
                </ButtonsContainer>
            </HeaderContainer>
        </HeaderSeparator>
    );
}

export default Header;