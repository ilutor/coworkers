import React from 'react';
import Header from './Header';
import { Global, css } from '@emotion/core';
import Head from 'next/head';
import PropTypes from 'prop-types';

const Layout = props => {

    return (

        <>
            <Global
                styles={css`
                :root{
                    --grey: #3d3d3d;
                    --grey2: #6F6F6F;
                    --grey3: hsla(237, 20%, 82%, 1);
                    --grey4: #d1d1d1;
                    --orange: hsla(237, 23%, 30%, 1);
                    --green: hsla(170,29%,60%,1);
                }
                html{
                    font-size:62.5%;
                    box-sizing:border-box;
                }
                *,*:before,*:after{
                    box-sizing:inherit;
                }
                body{
                    font-size:1.6rem;/*16px*/
                    line-height:1.5;
                    font-family: 'Fira Sans', sans-serif;
                    background-color:var(--grey3);
                }
                h1,h2,h3{
                    margin:0 0 0 0;
                    line-height:1.5;
                }
                h2{
                    font-family: 'Roboto Condensed', serif;
                    font-weight:700;
                    color: var(--orange);
                    font-size: 3rem;
                }
                h1{
                    font-weight:700;
                    color: var(--orange);
                    font-size: 5rem;
                }
                h3{
                    font-family: 'Fira Sans', sans-serif;
                }
                ul{
                    list-style:none;
                    margin:0;
                    padding:0;
                }
                a{
                    text-decoration:none;
                }
                *:focus{
                    outline: none;
                }

                footer{
                    position: fixed;
                    left: 0;
                    bottom: 0;
                    width: 100%;
                    text-align: center;
                    font-size:1.4rem;
                    background-color: hsla(170,29%,60%,1);
                    color: hsla(170, 29%, 38%, 1);
                    box-shadow: -2px -2px 2px;
                }
            `}
            />

            <Head>
                <title>CoWorkers Firebase & Next.js</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta property="og:title" content="CoWorkers Firebase & Next.js" key="title" />

                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha256-l85OmPOjvil/SOvVt3HnSSjzF1TUMyT9eV0c2BzEGzU=" crossOrigin="anonymous" />
                <link href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@400;700&family=Roboto+Condensed:wght@400;700&display=swap" rel="stylesheet" />
                <link href="/static/css/app.css" rel="stylesheet" />

            </Head>

            <Header />

            <main>
                {props.children}
            </main>
            <footer>Luis Torres - 2020</footer>
        </>

    );
}

Layout.propTypes = {
    children: PropTypes.object.isRequired
}

export default Layout;