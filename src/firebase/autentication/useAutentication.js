import { useEffect, useState } from 'react';
import firebase from '../firebase';

function useAutentication() {
    const [ userAutenticated, saveAutenticatedUser] = useState(null);

    useEffect(() => {
        const unsuscribe = firebase.auth.onAuthStateChanged(user => {
            if( user ) {
                saveAutenticatedUser(user);
            } else {
                saveAutenticatedUser(null);
            }
        });
        return () => unsuscribe();
    }, []);

    return userAutenticated;
}
export default useAutentication;