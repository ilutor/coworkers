import React from 'react';
import {render , cleanup} from '@testing-library/react';
import Error404 from '../components/layout/404';

afterEach(cleanup);

test("Error404 is Working ?", ()=> {
    const {getByText} = render(<Error404 />);
    expect(getByText("No se puede mostrar")).toBeInTheDocument();
});